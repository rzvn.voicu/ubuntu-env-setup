#!/bin/sh
sudo apt update
sudo apt upgrade -y
#Sublime
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
sudo apt-get install apt-transport-https -y
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
sudo apt-get update
sudo apt-get install sublime-text -y

#java
sudo apt install default-jre -y

# node and npm
sudo apt install nodejs npm -y
sudo npm install -g npm@7.4.2

#docker
sudo apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

read -p "Username: " user

sudo groupadd docker
sudo usermod -aG docker $user


#Docker Compose
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#Tilix
sudo apt install tilix -y

sudo apt install maven -y

mkdir -p ~/work/connectis


#Download idea
cd ~ && mkdir temp
cd ~/temp && wget -O ideaIU.tar.gz  https://download.jetbrains.com/idea/ideaIU-2020.3.1.tar.gz
sudo tar -xzf ~/temp/ideaIU.tar.gz -C /opt

cd /opt && cd `ls | grep idea`/bin && ./idea.sh


#oh my zsh
sudo apt install zsh -y
sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
