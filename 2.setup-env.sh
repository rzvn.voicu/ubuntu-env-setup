#!/bin/sh

# Gitlab token
read -p "GIT_TOKEN: " token

mkdir -p ~/.settings/

touch ~/.settings/env_export.sh

echo "export GITLAB_PRIVATE_TOKEN=$token" >> ~/.settings/env_export.sh
chmod +x  ~/.settings/env_export.sh

echo "source ~/.settings/env_export.sh" >> ~/.zshrc

#Docker login
read -p "email: " email

docker login -u $email -p $token registry.gitlab.com/connectiscom

#npm
npm config set @signicat:registry https://gitlab.com/api/v4/packages/npm/
npm config set '//gitlab.com/api/v4/packages/npm/:_authToken' "$token"

#ssh
mkdir ~/.ssh
cp ./public.txt ~/.ssh/id_rsa.pub
chmod 644 ~/.ssh/id_rsa.pub;
echo "" > ./public.txt

cp ./private.txt ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa;
echo "" > ./private.txt
ssh-add

#copy settings
mkdir ~/.m2
cp ./settings.xml ~/.m2/settings.xml

# trigger updates
zsh
